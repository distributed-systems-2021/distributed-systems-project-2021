package main.network;

import java.io.*;
import java.net.Socket;

public class SocketWrapper implements AutoCloseable {
	private final Socket socket;
	private final ObjectInputStream objectInputStream;
	private final ObjectOutputStream objectOutputStream;
	
	public SocketWrapper(Socket socket) throws IOException {
		this.socket = socket;
		this.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		this.objectInputStream = new ObjectInputStream(socket.getInputStream());
	}
	
	@Override
	public void close() throws IOException {
		objectOutputStream.close();
		socket.close();
	}
	
	public ObjectInputStream getObjectInputStream() {
		return objectInputStream;
	}
	
	public ObjectOutputStream getObjectOutputStream() {
		return objectOutputStream;
	}
	
}