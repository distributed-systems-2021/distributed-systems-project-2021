package main.execution;

import main.event_delivery.Producer;

public class ProducerMain {
	
	public static void main(String[] args) {
		Producer p = new Producer("SomeCoolChannel");
		p.initialize();
		
		p.push("Funny", "C://Users/Spyros/Desktop/Urrg.mp4", "Dogo goes urrg");
		p.push("Health", "C://Users/Spyros/Desktop/Sniz.mp4", "Dogo goes sniz");
		p.push("Horror", "C://Users/Spyros/Desktop/Arf.mp4", "Dogo goes arf");
		p.disconnect();
	}
	
}
