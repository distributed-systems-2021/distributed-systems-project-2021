package main.execution;

import main.event_delivery.broker.Broker;

public class AllBrokersMain {
	
	public static void main(String[] args) throws InterruptedException {
		for(int id = 1; id <= 3; id++) {
			Broker b = new Broker(id);
			b.initialize();
			
			Thread.sleep(5000);
		}
	}
	
}
