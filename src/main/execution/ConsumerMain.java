package main.execution;

import main.event_delivery.Consumer;

public class ConsumerMain {
	
	public static void main(String[] args) {
		Consumer c = new Consumer();
		c.initialize();
		String filename = "Urrg_video.mp4";
		String topicName = "topic name";
		String videoId = "8fb41fc8-0f48-497c-84ec-de690d0dfa4b";
		
		/*
		c.pull(videoId);
		c.queryByTopicName("Dogos");
		c.pull("random id");
		c.pull("1949ca82-90fb-4a31-8db7-27086ed7f586");
		*/
		
		c.queryByTopicName("Dogos");
		c.queryByChannelName("SomeCoolChannel");
		c.queryByChannelName("Some not existing channel");
		c.disconnect();
		
	}
	
}
