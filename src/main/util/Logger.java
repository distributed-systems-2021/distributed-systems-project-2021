package main.util;

public class Logger {
	
	public enum LogLevel {
		UNSPECIFIED,
		SILENT,
		ERROR,
		INFO,
		DEBUG
	}
	
	public static final LogLevel DEFAULT_LOG_LEVEL = LogLevel.INFO;
	private static LogLevel universalLogLevel = DEFAULT_LOG_LEVEL;
	private LogLevel individualLogLevel = LogLevel.UNSPECIFIED;
	
	private final String logSource;
	private static final String indentationUnit = "\t\t";
	private int nestingLevel = 0;
	private static final String NO_GROUP_MESSAGE = "Logger error: No group to end";
	private static final String INVALID_LOG_LEVEL_MESSAGE = "Logger error: Invalid log level";
	
	public Logger(String logSource) {
		this.logSource = logSource;
	}
	
	public void error(String output, Object... args) {
		if(shouldSuppressLevel(LogLevel.ERROR)) {
			return;
		}
		logf(LogLevel.ERROR, output, args);
	}
	
	public void exception(Exception e) {
		if(shouldSuppressLevel(LogLevel.ERROR))
			return;
		
		logf(LogLevel.ERROR, "Exception thrown:");
		e.printStackTrace();
	}
	
	public void info(String output, Object... args) {
		logf(LogLevel.INFO, output, args);
	}
	
	public void debug(String output, Object... args) {
		logf(LogLevel.DEBUG, output, args);
	}
	
	private void logf(LogLevel logLevel, String output, Object... args) {
		if(shouldSuppressLevel(logLevel))
			return;
		
		String indentation = indentationUnit.repeat(nestingLevel);
		String lineStart = logSource + " >   ";
		
		output = String.format(output, args);
		output = output.replace("\n", "\n" + " ".repeat(lineStart.length()) + indentation);
		
		System.out.println(lineStart + indentation + output);
	}
	
	private boolean shouldSuppressLevel(LogLevel logLevel) {
		LogLevel threshold = (individualLogLevel == LogLevel.UNSPECIFIED) ? universalLogLevel : individualLogLevel;
		
		return logLevel.compareTo(threshold) > 0;
	}
	
	public static void setUniversalLogLevel(LogLevel newUniversalLogLevel) {
		if(newUniversalLogLevel == LogLevel.UNSPECIFIED) {
			System.out.println(INVALID_LOG_LEVEL_MESSAGE);
			return;
		}
		
		universalLogLevel = newUniversalLogLevel;
	}
	
	public void overwriteUniversalLogLevel(LogLevel individualLogLevel) {
		this.individualLogLevel = individualLogLevel;
	}
	
	public void startGroup() {
		nestingLevel++;
	}
	
	public void endGroup() {
		if(nestingLevel == 0) {
			System.out.println(NO_GROUP_MESSAGE);
			return;
		}
		
		nestingLevel--;
	}
	
	public void endAllGroups() {
		nestingLevel = 0;
	}
	
}
