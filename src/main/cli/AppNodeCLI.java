package main.cli;

import main.event_delivery.Consumer;
import main.event_delivery.Producer;
import main.util.Logger;
import main.video.VideoInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class AppNodeCLI {
	private final BufferedReader userInputReader;
	private final Consumer consumer;
	private Producer producer = null;
	private String channelName = null;
	
	public static void main(String[] args) {
		Logger.setUniversalLogLevel(Logger.LogLevel.SILENT);
		
		new AppNodeCLI().start();
	}
	
	
	public AppNodeCLI() {
		this.userInputReader = new BufferedReader(new InputStreamReader(System.in));
		this.consumer = new Consumer();
	}
	
	public void start() {
		try {
			String command;
			consumer.initialize();
			while(true) {
				showMenu();
				command = userInputReader.readLine();
				
				switch(command) {
					case "1" -> queryByTopic();
					case "2" -> queryByChannel();
					case "3" -> uploadVideo();
					case "q" -> {
						quit();
						return;
					}
					default -> System.out.println("Unknown command " + command);
				}
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void uploadVideo() throws IOException {
		initializeProducer();
		System.out.println("Absolute path of the video to upload:");
		String path = userInputReader.readLine();
		System.out.println("Title for the video:");
		String name = userInputReader.readLine();
		System.out.println("Topic for the video:");
		String topic = userInputReader.readLine();
		
		boolean uploadSuccessful = producer.push(topic, path, name);
		
		if(uploadSuccessful)
			System.out.println("Upload successful\n");
		else
			System.out.println("Could not upload video\n");
		
	}
	
	private void quit() throws IOException {
		userInputReader.close();
		consumer.disconnect();
		if(producer != null)
			producer.disconnect();
		System.out.println("Quitting. Bye");
	}
	
	private void queryByChannel() throws IOException {
		System.out.println("Please enter a channel name");
		String channelName = userInputReader.readLine();
		List<VideoInfo> videos = consumer.queryByChannelName(channelName);
		
		handleQueryResults(videos);
	}
	
	private void initializeProducer() {
		if(producer != null)
			return;
		
		while(true) {
			System.out.println("Before continuing you must enter a channel name");
			try {
				channelName = userInputReader.readLine().trim();
				
				if(channelName.length() == 0) {
					System.out.println("Please enter a valid channel name");
					continue;
				}
				
				producer = new Producer(channelName);
				producer.initialize();
				return;
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void queryByTopic() throws IOException {
		System.out.println("Please enter a topic name");
		String topicName = userInputReader.readLine();
		List<VideoInfo> videos = consumer.queryByTopicName(topicName);
		
		handleQueryResults(videos);
	}
	
	private void handleQueryResults(List<VideoInfo> videos) throws IOException {
		if(videos.size() == 0) {
			System.out.println("No videos match your search\n");
			return;
		}
		
		for(int i = 0; i < videos.size(); i++) {
			System.out.println((i + 1) + ".\t" + getString(videos.get(i)));
		}
		promptToPull(videos);
		System.out.println();
	}
	
	private void promptToPull(List<VideoInfo> videos) throws IOException {
		while(true) {
			System.out.println("Enter the index of a video to download it or press enter to continue");
			
			String userInput = userInputReader.readLine();
			
			if(userInput.length() == 0)
				return;
			
			try {
				int selectedIndex = Integer.parseInt(userInput);
				
				if(selectedIndex < 1 || selectedIndex > videos.size()) {
					System.out.println(selectedIndex + " does not correspond to a video on the list");
					continue;
				}
				
				boolean pullSuccessful = consumer.pull(videos.get(selectedIndex - 1));
				if(pullSuccessful) {
					System.out.println("Saved video");
					return;
				}
				
				System.out.println("Failed to download video");
			}
			catch(NumberFormatException e) {
				System.out.println("Please enter a number");
			}
		}
	}
	
	private String getString(VideoInfo video) {
		return "\"" + video.getVideoName() + "\" by " + video.getChannelName();
	}
	
	private void showMenu() {
		System.out.println("Please choose a command");
		System.out.println("1: Query by Topic");
		System.out.println("2: Query by Channel");
		System.out.println("3: Upload a Video");
		
		System.out.println("q: Quit");
	}
	
}
