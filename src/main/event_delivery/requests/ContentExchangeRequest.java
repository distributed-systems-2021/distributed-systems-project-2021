package main.event_delivery.requests;

import main.event_delivery.broker.BrokerContent;

public class ContentExchangeRequest extends BaseRequest {
	
	private final int brokerId;
	private final BrokerContent content;
	
	public ContentExchangeRequest(int brokerId, BrokerContent content) {
		this.brokerId = brokerId;
		this.content = content;
	}
	
	public RequestType getType() {
		return RequestType.CONTENT_EXCHANGE_REQUEST;
	}
	
	public int getBrokerId() {
		return brokerId;
	}
	
	public BrokerContent getContent() {
		return content;
	}
	
}
