package main.event_delivery.requests;

public class PushRequest extends BaseRequest {
	
	private final String channelName;
	private final String videoName;
	private final String fileExtension;
	private final String topic;
	
	public PushRequest(String channelName, String videoName, String fileExtension, String topic) {
		this.channelName = channelName;
		this.videoName = videoName;
		this.fileExtension = fileExtension;
		this.topic = topic;
	}
	
	public String getTopic() {
		return topic;
	}
	
	public String getChannelName() { return channelName;}
	
	public String getVideoName() {
		return videoName;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}
	
	public RequestType getType() {
		return RequestType.PUSH_REQUEST;
	}
	
	@Override
	public String toString() {
		return "PushRequest{" +
				"channelName='" + channelName + '\'' +
				", videoName='" + videoName + '\'' +
				", fileExtension='" + fileExtension + '\'' +
				", topic='" + topic + '\'' +
				'}';
	}
	
}
