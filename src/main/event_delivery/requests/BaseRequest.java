package main.event_delivery.requests;

import java.io.Serializable;

abstract public class BaseRequest implements Serializable {
	
	public abstract RequestType getType();
	
}
