package main.event_delivery.requests;

import main.video.VideoInfo;

public class BroadcastNewContentRequest extends  BaseRequest {
	
	final VideoInfo newContentInfo;
	
	public BroadcastNewContentRequest(VideoInfo newContentInfo) {
		this.newContentInfo = newContentInfo;
	}
	
	public VideoInfo getNewContentInfo() {
		return newContentInfo;
	}
	
	@Override
	public RequestType getType() {
		return RequestType.BROADCAST_NEW_CONTENT_REQUEST;
	}
	
}
