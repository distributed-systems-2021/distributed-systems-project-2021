package main.event_delivery.requests;

public class PullRequest extends BaseRequest {
	
	private final String videoID;
	
	public PullRequest(String videoID) {
		this.videoID = videoID;
	}
	
	public RequestType getType() {
		return RequestType.PULL_REQUEST;
	}
	
	public String getVideoID() {
		return videoID;
	}
	
}
