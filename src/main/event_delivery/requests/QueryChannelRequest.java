package main.event_delivery.requests;

public class QueryChannelRequest extends BaseRequest {
	
	private final String channelName;
	
	public QueryChannelRequest(String channelName) {
		this.channelName = channelName;
	}
	
	@Override
	public RequestType getType() {
		return RequestType.QUERY_CHANNEL_REQUEST;
	}
	
	public String getChannelName() {
		return channelName;
	}
	
}
