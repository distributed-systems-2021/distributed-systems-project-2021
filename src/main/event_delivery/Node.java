package main.event_delivery;

import main.event_delivery.broker.BrokerTopology;
import main.util.HashGenerator;
import main.util.Logger;
import main.video.VideoInfo;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class Node {
	
	protected String storageDirectoryName;
	protected Logger logger;
	
	public abstract void initialize();
	
	protected BrokerTopology getTargetBroker(String diacritic, Map<Integer, BrokerTopology> knownBrokers) {
		BigInteger diacriticHash = HashGenerator.hash(diacritic);
		List<BrokerTopology> sortedTopologies = knownBrokers.values().stream().sorted(Comparator.comparing(BrokerTopology::getHashcode)).collect(Collectors.toList());
		
		// Output knownBroker hashes
		List<String> list = new ArrayList<>();
		for(BrokerTopology sortedTopology : sortedTopologies) {
			String s = String.valueOf(sortedTopology.getHashcode());
			list.add(s);
		}
		logger.info("Selecting target Broker");
		
		logger.startGroup();
		logger.info("Topic/channel of interest hash: " + diacriticHash);
		logger.info("Known Broker hashes: [%s]", String.join(", ", list));
		
		BrokerTopology selectedBroker = sortedTopologies.get(0);
		for(BrokerTopology topology : sortedTopologies) {
			if(diacriticHash.compareTo(topology.getHashcode()) < 0) {
				selectedBroker = topology;
				break;
			}
		}
		logger.info("Selecting broker %d with hash %d", selectedBroker.getId(), selectedBroker.getHashcode());
		logger.endGroup();
		
		return selectedBroker;
	}
	
	protected void createStorageDirectory() {
		Path storagePath = Paths.get(storageDirectoryName);
		try {
			Files.createDirectory(storagePath);
		}
		catch(FileAlreadyExistsException ignored) {
			logger.info("Skipping storage folder creation - folder already exists: " + storageDirectoryName);
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public void setLogLevel(Logger.LogLevel logLevel) {
		this.logger.overwriteUniversalLogLevel(logLevel);
	}
	
	public static String getDefaultDiacritic(VideoInfo videoInfo) {
		return videoInfo.getTopicName();
	}
	
}
