package main.event_delivery.broker;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;

public class BrokerContent implements Serializable {
	
	@Serial
	private static final long serialVersionUID = -1367934116535271462L;
	private final Set<String> topicNames;
	private final Set<String> channelNames;
	
	public BrokerContent(Set<String> topics, Set<String> channels) {
		this.topicNames = new HashSet<>(topics);
		this.channelNames = new HashSet<>(channels);
	}
	
	protected Set<String> getTopicNames() {
		return topicNames;
	}
	
	protected Set<String> getChannelNames() {
		return channelNames;
	}
	
	@Override
	public String toString() {
		return "BrokerContent{" + "\n" +
				"\t" + "topicNames=" + topicNames + "\n" +
				"\t" + "channelNames=" + channelNames + "\n" +
				'}';
	}
	
}
