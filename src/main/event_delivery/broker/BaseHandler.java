package main.event_delivery.broker;

import main.event_delivery.requests.DiscoveryRequest;
import main.util.Logger;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Map;

public abstract class BaseHandler implements Runnable {
	
	protected Broker parentBroker;
	protected Logger logger;
	
	protected void handleDiscoveryRequest(DiscoveryRequest request, ObjectOutputStream objectOutputStream) {
		Map<BrokerTopology, BrokerContent> brokersWithContent = parentBroker.getContentsByBroker();
		try {
			objectOutputStream.reset();
			objectOutputStream.writeObject(brokersWithContent);
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
}
