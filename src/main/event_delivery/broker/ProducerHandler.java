package main.event_delivery.broker;

import main.event_delivery.requests.DiscoveryRequest;
import main.util.Logger;
import main.util.StreamUtils;
import main.video.VideoInfo;
import main.event_delivery.requests.BaseRequest;
import main.event_delivery.requests.PushRequest;

import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.UUID;

class ProducerHandler extends BaseHandler {
	
	private final Socket socket;
	
	private static int handlerCounter = 0;
	private final int handlerId;
	private static synchronized int getNextId() {
		return ++handlerCounter;
	}
	
	public ProducerHandler(Broker parentBroker, Socket socket) {
		this.parentBroker = parentBroker;
		this.socket = socket;
		this.handlerId = getNextId();
		logger = new Logger("Broker " + parentBroker.getBrokerId() + " > ProdH " + handlerId);
	}
	
	@Override
	public void run() {
		try {
			ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			
			while(true) {
				logger.info("Waiting for new request");
				
				BaseRequest request = (BaseRequest) objectInputStream.readObject();
				logger.info("Got a %s", request.getType());
				logger.startGroup();
				switch(request.getType()) {
					case PUSH_REQUEST -> handlePushRequest((PushRequest) request, objectInputStream);
					case DISCOVERY_REQUEST -> handleDiscoveryRequest((DiscoveryRequest) request, objectOutputStream);
					case DISCONNECT_REQUEST -> {
						handleDisconnectRequest();
						return;
					}
				}
				logger.info("Request fulfilled: " + request.getType());
				logger.endGroup();
			}
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
			logger.info("Connection malfunction. ProducerHandler %d exiting", handlerId);
		}
		
	}
	
	private void handleDisconnectRequest() {
		logger.info("Producer has disconnected");
		try {
			closeConnection();
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void closeConnection() throws IOException {
		logger.info("Closing connection");
		this.socket.close();
	}
	
	private void handlePushRequest(PushRequest request, ObjectInputStream objectInputStream) {
		String fileId = generateFileId();
		logger.info("Incoming video info:");
		logger.startGroup();
		logger.info("Channel name: " + request.getChannelName());
		logger.info("Topic: " + request.getTopic());
		logger.info("Video name: " + request.getVideoName());
		logger.info("Video extension: " + request.getFileExtension());
		logger.info("File ID: " + fileId);
		logger.endGroup();
		
		File file = parentBroker.findVideoFile(fileId, request.getFileExtension());
		try(FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			logger.info("Receiving video");
			StreamUtils.copyChunks(objectInputStream, fileOutputStream);
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
			return;
		}
		
		
		Date currentDate = new Date();
		logger.info("Video received (on %s)", currentDate);
		VideoInfo videoInfo = new VideoInfo(fileId, request.getVideoName(), request.getFileExtension(), request.getChannelName(), request.getTopic(), currentDate, parentBroker.getBrokerId());
		parentBroker.addVideoToMaps(videoInfo);
		
		logger.info("Broadcasting new content:");
		logger.startGroup();
		parentBroker.broadcastInfo(videoInfo);
		logger.endGroup();
	}
	
	private String generateFileId() {
		return UUID.randomUUID().toString();
	}
	
}
