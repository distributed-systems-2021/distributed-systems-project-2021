package main.event_delivery.broker;

import main.network.SocketWrapper;
import main.util.Logger;
import main.event_delivery.Node;
import main.video.VideoInfo;
import main.video.VideoInfoEncoder;
import main.event_delivery.requests.*;

import java.io.*;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class Broker extends Node {
	
	private final int brokerId;
	private final BrokerTopology topology;
	private Map<String, List<String>> videosByChannelName = new HashMap<>();
	private Map<String, List<String>> videosByTopic = new HashMap<>();
	private Map<String, VideoInfo> videosById = new HashMap<>();
	private final Map<Integer, BrokerContent> contentsByBrokerId = new HashMap<>();
	private static final String CACHE_FILE_NAME = "_cache.txt";
	private ServerSocket socketForProducers;
	private ServerSocket socketForConsumers;
	private ServerSocket socketForBrokers;
	
	private final HashMap<Integer, SocketWrapper> brokerSocketsById = new HashMap<>();
	
	
	public static final String localIp = "192.168.2.100";
	
	private static final Map<Integer, BrokerTopology> STANDARD_BROKERS = Stream.of(
			new BrokerTopology(1, localIp, 5001, 6001, 7001),
			new BrokerTopology(2, localIp, 5002, 6002, 7002),
			new BrokerTopology(3, localIp, 5003, 6003, 7003)
	).collect(toMap(BrokerTopology::getId, identity()));
	
	
	public Broker(int brokerId) {
		this.brokerId = brokerId;
		this.topology = STANDARD_BROKERS.get(brokerId);
		storageDirectoryName = "Storage_Broker_" + this.brokerId;
		logger = new Logger("Broker " + brokerId);
	}
	
	@Override
	public void initialize() {
		logger.info("Initializing");
		createStorageDirectory();
		restoreVideoCache();
		
		new Thread(this::listenForBrokers).start();
		exchangeInfoWithAllOtherBrokers();
		
		new Thread(this::listenForProducers).start();
		new Thread(this::listenForConsumers).start();
	}
	
	private void restoreVideoCache() {
		try(BufferedReader bufferedReader = new BufferedReader(new FileReader(getCacheFile()))) {
			List<VideoInfo> videos = bufferedReader.lines().map(VideoInfoEncoder::decode).collect(toList());
			videosById = videos.stream().collect(toMap(VideoInfo::getId, identity()));
			videosByTopic = videos.stream().collect(groupingBy(VideoInfo::getTopicName, mapping(VideoInfo::getId, toList())));
			videosByChannelName = videos.stream().collect(groupingBy(VideoInfo::getChannelName, mapping(VideoInfo::getId, toList())));
		}
		catch(FileNotFoundException e) {
			logger.info("Cache backup file not found");
		}
		catch(IOException e) {
			logger.info("Failed to restore cache from file");
			logger.exception(e);
		}
		contentsByBrokerId.put(brokerId, getOwnContent());
	}
	
	private void listenForConsumers() {
		try {
			socketForConsumers = new ServerSocket(topology.getPortForConsumers());
			
			logger.info("Listening for consumers on port %d", topology.getPortForConsumers());
			while(true) {
				Socket socket = socketForConsumers.accept();
				logger.info("Got a Consumer connection");
				new Thread(new ConsumerHandler(this, socket)).start();
			}
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void listenForProducers() {
		try {
			socketForProducers = new ServerSocket(topology.getPortForProducers());
			
			logger.info("Listening for producers on port %d", topology.getPortForProducers());
			while(true) {
				Socket socket = socketForProducers.accept();
				logger.info("Got a Producer connection");
				new Thread(new ProducerHandler(this, socket)).start();
			}
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void listenForBrokers() {
		try {
			socketForBrokers = new ServerSocket(topology.getPortForBrokers());
			logger.info("Listening for brokers on port %d", topology.getPortForBrokers());
			while(true) {
				Socket socket = socketForBrokers.accept();
				SocketWrapper socketWrapper = new SocketWrapper(socket);
				logger.info("Got a Broker connection. Creating the corresponding handler");
				new Thread(new BrokerHandler(this, socketWrapper)).start();
			}
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void exchangeInfoWithAllOtherBrokers() {
		for(BrokerTopology target : STANDARD_BROKERS.values()) {
			if(target.getId() == this.brokerId)
				continue;
			
			try {
				SocketWrapper socketWrapper = new SocketWrapper(
						new Socket(target.getIp(), target.getPortForBrokers())
				);
				
				brokerSocketsById.put(target.getId(), socketWrapper);
				logger.info("Reached Broker " + target.getId());
				
				ObjectOutputStream objectOutputStream = socketWrapper.getObjectOutputStream();
				
				objectOutputStream.writeObject(new ContentExchangeRequest(brokerId, getOwnContent()));
				logger.info("Sent own content to Broker " + target.getId());
				
				ObjectInputStream objectInputStream = socketWrapper.getObjectInputStream();
				BrokerContent otherBrokerContent = (BrokerContent) objectInputStream.readObject();
				logger.info("Received back content from Broker " + target.getId());
				logger.startGroup();
				logger.info("Topics: ");
				logger.info(otherBrokerContent.getTopicNames().toString());
				logger.info("Channels: ");
				logger.info(otherBrokerContent.getChannelNames().toString());
				logger.endGroup();
				
				logger.info("Creating a BrokerHandler for any further interaction with Broker %d", target.getId());
				new Thread(new BrokerHandler(this, socketWrapper)).start();
			}
			catch(ConnectException e) {
				logger.info("Failed to reach broker " + target.getId());
			}
			catch(IOException | ClassNotFoundException e) {
				logger.exception(e);
			}
		}
	}
	
	protected BrokerContent getOwnContent() {
		return new BrokerContent(videosByTopic.keySet(), videosByChannelName.keySet());
	}
	
	protected synchronized List<VideoInfo> getVideosByChannelName(String channelName) {
		if(!videosByChannelName.containsKey(channelName)) {
			return new ArrayList<>();
		}
		return videosByChannelName.get(channelName).stream()
				.map(id -> videosById.get(id))
				.collect(toList());
	}
	
	protected synchronized VideoInfo getVideoById(String id) {
		return videosById.get(id);
	}
	
	protected synchronized List<VideoInfo> getVideosByTopic(String topicName) {
		if(!videosByTopic.containsKey(topicName)) {
			return new ArrayList<>();
		}
		return videosByTopic.get(topicName).stream()
				.map(id -> videosById.get(id))
				.collect(toList());
	}
	
	protected synchronized void addVideoToMaps(VideoInfo videoInfo) {
		videosById.put(videoInfo.getId(), videoInfo);
		
		if(videosByChannelName.containsKey(videoInfo.getChannelName()))
			videosByChannelName.get(videoInfo.getChannelName()).add(videoInfo.getId());
		else
			videosByChannelName.put(videoInfo.getChannelName(), new ArrayList<>() {{ add(videoInfo.getId()); }});
		
		if(videosByTopic.containsKey(videoInfo.getTopicName()))
			videosByTopic.get(videoInfo.getTopicName()).add(videoInfo.getId());
		else
			videosByTopic.put(videoInfo.getTopicName(), new ArrayList<>() {{ add(videoInfo.getId()); }});
		
		contentsByBrokerId.get(brokerId).getChannelNames().add(videoInfo.getChannelName());
		contentsByBrokerId.get(brokerId).getTopicNames().add(videoInfo.getTopicName());
		
		saveVideoPersistent(videoInfo);
	}
	
	private synchronized void saveVideoPersistent(VideoInfo videoInfo) {
		File cacheFile = getCacheFile();
		try(
				FileWriter fileWriter = new FileWriter(cacheFile, true);
				BufferedWriter bw = new BufferedWriter(fileWriter)
		) {
			String videoDescription = VideoInfoEncoder.encode(videoInfo);
			bw.write(videoDescription);
			bw.newLine();
		}
		catch(IOException e) {
			logger.exception(e);
		}
		
	}
	
	private File getCacheFile() {
		return Path.of(storageDirectoryName, CACHE_FILE_NAME).toFile();
	}
	
	protected synchronized File findVideoFile(String id, String fileExtension) {
		String filename = id + "." + fileExtension;
		return Path.of(storageDirectoryName, filename).toFile();
	}
	
	protected synchronized File findVideoFile(VideoInfo videoInfo) {
		return findVideoFile(videoInfo.getId(), videoInfo.getFileExtension());
	}
	
	protected int getBrokerId() {
		return brokerId;
	}
	
	protected synchronized Map<BrokerTopology, BrokerContent> getContentsByBroker() {
		return contentsByBrokerId.entrySet().stream().collect(toMap(
				(keyValuePair) -> Broker.STANDARD_BROKERS.get(keyValuePair.getKey()),
				Map.Entry::getValue));
	}
	
	protected synchronized void updateBrokerContent(int brokerId, BrokerContent content) {
		contentsByBrokerId.put(brokerId, content);
	}
	
	protected synchronized void broadcastInfo(VideoInfo videoInfo) {
		SocketWrapper socketWrapper;
		BroadcastNewContentRequest request = new BroadcastNewContentRequest(videoInfo);
		for(Integer id : brokerSocketsById.keySet()) {
			try {
				socketWrapper = brokerSocketsById.get(id);
				logger.info("Sending information to Broker %d", id);
				socketWrapper.getObjectOutputStream().writeObject(request);
			}
			catch(IOException e) {
				logger.exception(e);
			}
		}
		
	}
	
	protected synchronized void addToBrokerSockets(int brokerId, SocketWrapper socketWrapper) {
		this.brokerSocketsById.put(brokerId, socketWrapper);
	}
	
}
