package main.event_delivery.broker;

import main.event_delivery.requests.*;
import main.network.SocketWrapper;
import main.util.Logger;

import java.io.*;

class BrokerHandler implements Runnable {
	
	private final Broker parentBroker;
	private final SocketWrapper socketWrapper;
	private final Logger logger;
	
	private static int brokerHandlerCounter = 0;
	
	private static synchronized int getNextId() {
		return ++brokerHandlerCounter;
	}
	
	public BrokerHandler(Broker parentBroker, SocketWrapper socketWrapper) {
		this.parentBroker = parentBroker;
		this.socketWrapper = socketWrapper;
		this.logger = new Logger("Broker " + parentBroker.getBrokerId() + " > BrokH " + getNextId());
	}
	
	@Override
	public void run() {
		loop:
		while(true) {
			
			logger.info("Waiting for new request");
			
			try {
				ObjectInputStream objectInputStream = socketWrapper.getObjectInputStream();
				ObjectOutputStream objectOutputStream = socketWrapper.getObjectOutputStream();
				BaseRequest request = (BaseRequest) objectInputStream.readObject();
				
				logger.info("Got a " + request.getType());
				logger.startGroup();
				switch(request.getType()) {
					case CONTENT_EXCHANGE_REQUEST -> handleInfoExchangeRequest((ContentExchangeRequest) request, objectOutputStream);
					case BROADCAST_NEW_CONTENT_REQUEST -> handleBroadcastRequest((BroadcastNewContentRequest) request);
					//case DISCOVERY_REQUEST -> logger.info(parentBroker.getBrokerId() + ": Got a discovery request from another broker");
					case DISCONNECT_REQUEST -> {handleDisconnectRequest((DisconnectRequest) request); break loop;}	// Just in case
				}
				
				logger.info("Request fulfilled: " + request.getType());
				logger.endGroup();
			}
			catch(IOException | ClassNotFoundException e) {
				logger.exception(e);
				logger.info("Connection malfunction. Retrying");
			}
		}
		
	}
	
	private void handleDisconnectRequest(DisconnectRequest request) {
		logger.info("Disconnect request received");
	}
	
	private void handleBroadcastRequest(BroadcastNewContentRequest request) {
		logger.info("Received content broadcast. New content:");
		logger.startGroup();
		logger.info(request.getNewContentInfo().toString());
		logger.endGroup();
		
		parentBroker.addVideoToMaps(request.getNewContentInfo());
	}
	
	private void handleInfoExchangeRequest(ContentExchangeRequest request, ObjectOutputStream objectOutputStream) {
		parentBroker.addToBrokerSockets(request.getBrokerId(), this.socketWrapper);		//Todo Dedicate a request for this?
		
		parentBroker.updateBrokerContent(request.getBrokerId(), request.getContent());
		logger.info("Got info from Broker " + request.getBrokerId());
		
		logger.startGroup();
		logger.info("Topics: ");
		logger.info(request.getContent().getTopicNames().toString());
		logger.info("Channels: ");
		logger.info(request.getContent().getChannelNames().toString());
		logger.endGroup();
		
		BrokerContent content = parentBroker.getOwnContent();
		try {
			objectOutputStream.writeObject(content);
			logger.info("Replied by sending own content to Broker " + request.getBrokerId());
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
}
