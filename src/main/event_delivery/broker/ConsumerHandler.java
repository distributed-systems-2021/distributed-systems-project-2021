package main.event_delivery.broker;

import main.util.Logger;
import main.util.StreamUtils;
import main.video.VideoInfo;
import main.event_delivery.requests.*;

import java.io.*;
import java.net.Socket;
import java.util.List;

class ConsumerHandler extends BaseHandler {
	
	private final Socket socket;
	
	private static int consumerHandlerCounter = 0;
	private final int handlerId;
	
	private static synchronized int getNextId() {
		return ++consumerHandlerCounter;
	}
	
	public ConsumerHandler(Broker parentBroker, Socket socket) {
		this.parentBroker = parentBroker;
		this.socket = socket;
		this.handlerId = getNextId();
		this.logger = new Logger("Broker " + parentBroker.getBrokerId() + " > ConsH " + this.handlerId);
	}
	
	@Override
	public void run() {
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
			
			while(true) {
				logger.info("Waiting for new request");
				
				BaseRequest request = (BaseRequest) objectInputStream.readObject();
				
				logger.info("Got a " + request.getType());
				logger.startGroup();
				switch(request.getType()) {
					case PULL_REQUEST -> handlePullRequest((PullRequest) request, objectOutputStream);
					case QUERY_CHANNEL_REQUEST -> handleQueryChannelRequest((QueryChannelRequest) request, objectOutputStream);
					case QUERY_TOPIC_REQUEST -> handleQueryTopicRequest((QueryTopicRequest) request, objectOutputStream);
					case DISCOVERY_REQUEST -> handleDiscoveryRequest((DiscoveryRequest) request, objectOutputStream);
					case DISCONNECT_REQUEST -> {
						handleDisconnectRequest();
						return;
					}
				}
				logger.info("Request fulfilled: " + request.getType());
				logger.endGroup();
			}
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
			logger.info("Connection malfunction. ConsumerHandler %d exiting", handlerId);
		}
	}
	
	private void handleDisconnectRequest() {
		logger.info("Consumer has disconnected");
		try {
			this.socket.close();
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void handleQueryChannelRequest(QueryChannelRequest request, ObjectOutputStream objectOutputStream) {
		logger.info("Query is for channel name \"%s\"", request.getChannelName());
		List<VideoInfo> videos = parentBroker.getVideosByChannelName(request.getChannelName());
		try {
			logger.info("Sending query results");
			objectOutputStream.writeObject(videos);
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void handleQueryTopicRequest(QueryTopicRequest request, ObjectOutputStream objectOutputStream) {
		logger.info("Query is for topic \"%s\"", request.getTopicName());
		List<VideoInfo> videos = parentBroker.getVideosByTopic(request.getTopicName());
		try {
			logger.info("Sending query results");
			objectOutputStream.writeObject(videos);
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
	private void handlePullRequest(PullRequest request, ObjectOutputStream objectOutputStream) {
		String videoID = request.getVideoID();
		logger.info("Requested video ID: %s", videoID);
		
		VideoInfo videoInfo = parentBroker.getVideoById(videoID);
		
		boolean videoExists = (videoInfo != VideoInfo.INVALID);
		boolean parentIsResponsible = videoExists && (videoInfo.getResponsibleBrokerId() == parentBroker.getBrokerId());
		
		logger.info("Sending info of the requested video");
		try {
			if(!videoExists) {
				logger.info("The file requested by the consumer does not exist - sending INVALID (null)");
				objectOutputStream.writeObject(VideoInfo.INVALID);
			}
			else if(!parentIsResponsible) {
				logger.info("The file requested exists, but not in Broker %d - sending INVALID", parentBroker.getBrokerId());
				objectOutputStream.writeObject(VideoInfo.INVALID);
			}
			else
				objectOutputStream.writeObject(videoInfo);
		}
		catch(IOException e) {
			logger.exception(e);
		}
		
		if(!videoExists || !parentIsResponsible)
			return;
		
		File requestedFile = parentBroker.findVideoFile(videoInfo);
		try(
				FileInputStream fileInputStream = new FileInputStream(requestedFile)
		) {
			logger.info("Sending video...");
			StreamUtils.copyChunks(fileInputStream, objectOutputStream);
		}
		catch(IOException e) {
			logger.exception(e);
		}
	}
	
}
