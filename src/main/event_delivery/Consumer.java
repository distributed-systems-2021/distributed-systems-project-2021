package main.event_delivery;

import main.event_delivery.broker.BrokerTopology;
import main.network.SocketWrapper;
import main.util.StreamUtils;
import main.video.VideoInfo;
import main.util.Logger;
import main.event_delivery.requests.PullRequest;
import main.event_delivery.requests.QueryChannelRequest;
import main.event_delivery.requests.QueryTopicRequest;

import java.io.*;
import java.nio.file.Path;
import java.util.List;

public class Consumer extends ClientNode {
	
	public static final String STORAGE_DIRECTORY_NAME = "Downloads";
	
	public Consumer() {
		logger = new Logger("Consumer");
	}
	
	@Override
	public void initialize() {
		storageDirectoryName = STORAGE_DIRECTORY_NAME;
		createStorageDirectory();
		sendDiscoveryRequest();
	}
	
	public List<VideoInfo> queryByChannelName(String channelName) {
		SocketWrapper wrapper = connectToProperBrokerExplicit(channelName);
		if(wrapper == null) return null;
		
		try {
			ObjectOutputStream objectOutputStream = wrapper.getObjectOutputStream();
			ObjectInputStream objectInputStream = wrapper.getObjectInputStream();
			QueryChannelRequest request = new QueryChannelRequest(channelName);
			objectOutputStream.writeObject(request);
			
			@SuppressWarnings("unchecked")
			List<VideoInfo> videoInfos = (List<VideoInfo>) objectInputStream.readObject();
			logger.info("Query results for channel \"" + channelName + "\"");
			for(VideoInfo videoInfo : videoInfos) {
				logger.info(videoInfo.toString());
			}
			return videoInfos;
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return null;
	}
	
	public List<VideoInfo> queryByTopicName(String topicName) {
		SocketWrapper wrapper = connectToProperBrokerExplicit(topicName);
		if(wrapper == null) return null;
		
		try {
			ObjectOutputStream objectOutputStream = wrapper.getObjectOutputStream();
			ObjectInputStream objectInputStream = wrapper.getObjectInputStream();
			QueryTopicRequest request = new QueryTopicRequest(topicName);
			objectOutputStream.writeObject(request);
			
			@SuppressWarnings("unchecked")
			List<VideoInfo> videoInfos = (List<VideoInfo>) objectInputStream.readObject();
			logger.info("Query results for topic \"" + topicName + "\"");
			
			for(VideoInfo videoInfo : videoInfos) {
				logger.info(videoInfo.toString());
			}
			
			return videoInfos;
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return null;
	}
	
	public boolean pull(VideoInfo targetVideo) {
		BrokerTopology targetBrokerTopology = getBrokerFor(targetVideo);
		SocketWrapper wrapper = connectToBroker(targetBrokerTopology);
		if(wrapper == null) return false;
		
		try {
			ObjectOutputStream objectOutputStream = wrapper.getObjectOutputStream();
			ObjectInputStream objectInputStream = wrapper.getObjectInputStream();
			PullRequest request = new PullRequest(targetVideo.getId());
			objectOutputStream.writeObject(request);
			
			VideoInfo videoInfo = (VideoInfo) objectInputStream.readObject();
			if(videoInfo == VideoInfo.INVALID) {
				logger.info("The requested video does not exist");
				return false;
			}
			
			FileOutputStream fileOutputStream = new FileOutputStream(getFileSaveLocation(videoInfo));
			StreamUtils.copyChunks(objectInputStream, fileOutputStream);
			fileOutputStream.close();
			return true;
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return false;
	}
	
	private File getFileSaveLocation(VideoInfo videoInfo) {
		return Path.of(storageDirectoryName, videoInfo.getVideoName() + "." + videoInfo.getFileExtension()).toFile();
	}
	
	@Override
	protected BrokerTopology getBrokerFor(VideoInfo targetVideo) {
		return knownBrokers.get(targetVideo.getResponsibleBrokerId());
	}
	
	@Override
	protected int getCorrespondingPort(BrokerTopology topology) {
		return topology.getPortForConsumers();
	}
	
}

