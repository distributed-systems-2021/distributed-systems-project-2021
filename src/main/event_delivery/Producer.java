package main.event_delivery;

import main.event_delivery.broker.BrokerTopology;
import main.network.SocketWrapper;
import main.util.StreamUtils;
import main.util.Logger;
import main.event_delivery.requests.PushRequest;
import main.video.VideoInfo;

import java.io.*;

public class Producer extends ClientNode {
	
	private final String channelName;
	
	public Producer(String channelName) {
		this.channelName = channelName;
		logger = new Logger("Producer");
	}
	
	@Override
	public void initialize() {
		sendDiscoveryRequest();
	}
	
	/**
	 * @param topicName The name of the video's topic - defined by the user
	 * @param filePath  The location of the video file on the local machine
	 * @param videoName The name with which the video will be uploaded
	 */
	public boolean push(String topicName, String filePath, String videoName) {
		VideoInfo partialVideoInfo = new VideoInfo();
		partialVideoInfo.setVideoName(videoName);
		partialVideoInfo.setChannelName(this.channelName);
		partialVideoInfo.setTopicName(topicName);
		
		BrokerTopology targetBrokerTopology = getBrokerFor(partialVideoInfo);
		SocketWrapper connection = connectToBroker(targetBrokerTopology);
		if(connection == null)
			return false;
		
		logger.info("Connected to broker at %s:%d", targetBrokerTopology.getIp(), targetBrokerTopology.getPortForProducers());
		
		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);
			String delimiters = File.separator + ".";
			String[] filePathParts = filePath.split(delimiters);
			String fileExtension = filePathParts[filePathParts.length - 1];
			
			PushRequest request = new PushRequest(this.channelName, videoName, fileExtension, topicName);
			logger.info("Sending push request: " + request);
			connection.getObjectOutputStream().writeObject(request);
			connection.getObjectOutputStream().flush();
			
			logger.startGroup();
			logger.info("Pushing video...");
			
			StreamUtils.copyChunks(fileInputStream, connection.getObjectOutputStream());
			
			logger.info("Done pushing video");
			logger.endGroup();
			return true;
		}
		catch(IOException e) {
			logger.exception(e);
		}
		return false;
	}
	
	@Override
	protected BrokerTopology getBrokerFor(VideoInfo targetVideo) {
		String diacritic = getDefaultDiacritic(targetVideo);
		
		if(diacritic == null) {
			logger.info("Could not decide where to send the video %s", targetVideo);
			return null;
		}
		
		return getTargetBroker(diacritic, knownBrokers);
	}
	
	@Override
	protected int getCorrespondingPort(BrokerTopology topology) {
		return topology.getPortForProducers();
	}
	
}
